Source: boogie
Section: cli-mono
Priority: optional
Maintainer: Benjamin Barenblat <bbaren@mit.edu>
Build-Depends: debhelper-compat (= 12),
               cli-common-dev,
               mono-devel,
               tzdata
Standards-Version: 4.4.1
Homepage: http://research.microsoft.com/en-us/projects/boogie/
Vcs-Browser: https://salsa.debian.org/debian/boogie
Vcs-Git: https://salsa.debian.org/debian/boogie.git

Package: boogie
Architecture: all
Depends:
 libboogie-cil (= ${binary:Version}),
 mono-mcs,
 z3,
 ${cli:Depends},
 ${misc:Depends}
Suggests:
 libgtk2.0-0,
Description: verifiable programming language (compiler)
 Boogie is a compiler intermediate language with support for automatic invariant
 checking using an SMT solver such as Z3.  It supports program verification for
 a variety of other, higher-level languages, including Spec\#, C, Dafny, and
 Chalice.
 .
 This package contains the Boogie compiler, as well as bvd, the Boogie
 Verification Debugger.

Package: libboogie-cil
Architecture: all
Depends: ${cli:Depends}, ${misc:Depends}
Description: verifiable programming language (library)
 Boogie is a compiler intermediate language with support for automatic invariant
 checking using an SMT solver such as Z3.  It supports program verification for
 a variety of other, higher-level languages, including Spec\#, C, Dafny, and
 Chalice.
 .
 This package contains the Boogie library.
